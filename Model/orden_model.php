<?php

require_once('database.php');

class OrdenModel
{
    private $db;
    private $orden;
    private $mesero;
    private $folio;


    public function __construct()
    {
        $this->db = Conexion::conectar();
        $this->orden = $_POST['comanda'];
        session_start();
        $this->mesero = $_SESSION['mesero'];
        session_write_close();
        $this->folio = array();
    }

    public function insertar_orden()
    {
        $qry = $this->db->prepare("INSERT INTO ordenes (comanda, horaEntrada, mesero_id, status)
        VALUES ('$this->orden', CURTIME(), '$this->mesero', 'activo');");

        return $qry->execute();
    }

    public function consultar_folios(){
        $qry = $this->db->prepare("SELECT folio FROM ordenes WHERE mesero = '$this->mesero' AND status='activo'");
        return $qry->execute();
        $rows = $qry->rowCount();
      if ($rows > 0) {
          $this->folio = $qry->fetch(PDO::FETCH_ASSOC);
      }
      return $this->folio;
        
    }
}

?>
