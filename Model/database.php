<?php

class Conexion{

  public static function conectar(){
    
    $conexion = array();

    try {

      $conexion = new PDO('mysql:dbname=Comandero;host=127.0.0.1', 'root', '123456');
      $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $conexion->exec("set names utf8");
    } catch (PDOException $e) {
            
      echo 'Error al conectarnos'. $e -> getMessage();

    }

    return $conexion;


  }
}
