<?php
require_once('database.php');
class AuthModel{

  private $db;
  private $mesero;

  public function __construct(){
    $this->db = Conexion::conectar();
    $this->mesero = array();
  }

  public function auth_mesero($mesero_id){
      $qry = $this->db->prepare('SELECT nombre, pass FROM meseros WHERE mesero = ?;');
      $qry->execute(array($mesero_id));
      $rows = $qry->rowCount();
      if ($rows > 0) {
          $this->mesero = $qry->fetch(PDO::FETCH_ASSOC);
      }
      return $this->mesero;
  }

}

?>