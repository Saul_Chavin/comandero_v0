<?php

require_once('database.php');

class EditarModel
{
    private $db;
    private $mesero;
    private $folio;
    private $orden;
    private $editada;
    private $folioSelec;


    public function __construct(){
    
        $this->db = Conexion::conectar();
        $this->editada = $_POST['editada'];
        $this->folioSelec = $_POST['folio'];
        session_start();
        $this->mesero = $_SESSION['mesero'];
        session_write_close();
        $this->folio = array();
        $this->orden = array();
    }

    public function consultar_folios(){
        $row= $this->db->query("SELECT folio, comanda FROM ordenes WHERE mesero_id = '$this->mesero' AND status='activo';");
        foreach($row as $folio){
            $this->folio[] = $folio['folio'];
        }
       
            return $this->folio;
          
        
    }

    public function consultar_orden(){
        $row= $this->db->query("SELECT comanda FROM ordenes WHERE mesero_id = '$this->mesero' AND status='activo'");
        foreach($row as $comanda){
            $this->orden[] = $comanda['comanda'];
        }
       
            return $this->orden;

    }

    public function actualizar_orden(){
        $qry = $this->db->prepare("UPDATE ordenes SET comanda = '$this->editada' WHERE folio = '$this->folioSelec';");

        return $qry->execute();
    }
}
?>