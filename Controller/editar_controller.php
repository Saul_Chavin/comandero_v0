<?php
session_start();
//Llamada al modelo
require_once("../Model/editar_model.php");
$edicion = new EditarModel();
$folios;
$ordenes;

if (!isset($_SESSION['editar'])) {
  $folios = $edicion->consultar_folios();
  $ordenes = $edicion->consultar_orden();/*  */
  if ($folios != 0 and $ordenes != 0) {
      require_once('../editar_comandas.php');
      $_SESSION['editar'] = $ordenes;
      session_write_close();
      // header('Location: ../editar_comandas.php');
  }
}else{
  if($edicion->actualizar_orden()){
    unset($_SESSION['editar']);
    session_write_close();
    require_once('../comandas.php');

  }
}

?>