<div class="container" id="cervezas">
  <h1>Cervezas</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Corona">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Corona</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Modelo">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Modelo</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Victoria">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Victoria</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Pacifico">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Pacífico</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Ultra">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Ultra</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Artesanal">
        <img src="../Views/img/beer.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="bebida">Artesanal</h4>
      </button>
    </div>
  </div>
</div>

<div class="container" id="refrescos">
  <h1>Refrescos</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Coca">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title" id="bebida">Coca Cola</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Sprite">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title">Sprite</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Fanta">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title">Fanta</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Manzanita">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title">Manzanita</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Fresca">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title">Fresca</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Coca Light">
        <img src="../Views/img/soda.png" class="card-img-top" alt="...">
        <h4 class="card-title">Coca Light</h4>
      </button>
    </div>
  </div>
</div>

<div class="container" id="preparadas">
  <h1>Preparadas</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Mojito">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Mojito</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Margarita">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Margarita</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Piña Colada">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Piña Colada</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Sangria">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Sangría</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Naranjada">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Naranjada</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Limonada">
        <img src="../Views/img/mojito.png" class="card-img-top" alt="...">
        <h4 class="card-title">Limonada</h4>
      </button>
    </div>
  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_success" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal-title">
          <!-- js inner -->
        </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-8">
            <p id="modal-p"></p>
          </div>
          <div class="col-4" id="modal-icon">
            <!-- el contenido se carga desde javascript: comandas_menu.js -->
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>




<div class="container" id="pizzas">
  <h1>Pizzas</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Hawaiana">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Hawaiana</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Carnes Frías">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Carnes Frías</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Salami">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Salami</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Croccante">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Croccante</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Cielo Mar y Tierra">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Cielo Mar y Tierra</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pizza Vegetariana">
        <img src="../Views/img/pizza.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="platillo">Pizza Vegetariana</h4>
      </button>
    </div>
  </div>



  <div class="container" id="pastas">
    <h1>Pastas</h1>
    <div class="row">
      <div class="col-6 col-sm-6 center">
        <button class="card bg-warning center" id="btnBebida" value="Pasta Alfredo">
          <img src="../Views/img/pasta.png" class="card-img-top icard" alt="...">
          <h4 class="card-title" id="platillo">Pasta Alfredo</h4>
        </button>
      </div>
      <div class="col-6 col-sm-6 center">
        <button class="card bg-warning center" id="btnBebida" value="Pasta Pesto">
          <img src="../Views/img/pasta.png" class="card-img-top icard" alt="...">
          <h4 class="card-title" id="platillo">Pasta Pesto</h4>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-6 col-sm-6 center">
        <button class="card bg-warning center" id="btnBebida" value="Fetuccini">
          <img src="../Views/img/pasta.png" class="card-img-top icard" alt="...">
          <h4 class="card-title" id="platillo">Fetuccini</h4>
        </button>
      </div>
      <div class="col-6 col-sm-6 center">
        <button class="card bg-warning center" id="btnBebida" value="Spaghetti verde">
          <img src="../Views/img/pasta.png" class="card-img-top icard" alt="...">
          <h4 class="card-title" id="platillo">Spaghetti verde</h4>
        </button>
      </div>
    </div>



    <div class="container" id="ensaladas">
      <h1>Ensaladas</h1>
      <div class="row">
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada Cesar">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada Cesar</h4>
          </button>
        </div>
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada Pasta">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada Pasta</h4>
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada verde">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada verde</h4>
          </button>
        </div>
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada Griega">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada Griega</h4>
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada de Manzana">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada de Manzana</h4>
          </button>
        </div>
        <div class="col-6 col-sm-6 center">
          <button class="card bg-dark text-white center" id="btnBebida" value="Ensalada Alfredo">
            <img src="../Views/img/ensalada.png" class="card-img-top icard" alt="...">
            <h4 class="card-title" id="platillo">Ensalada Alfredo</h4>
          </button>
        </div>
      </div>


      <div class="container" id="vegetariana">
  <h1>Entradas Vegetarianas</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Vegetales a vapor">
        <img src="Views/img/vegetal.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Vegetales a Vapor</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Rollos de lechuga">
        <img src="Views/img/vegetal.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Rollos de lechuga</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Tarta de vegetales">
        <img src="Views/img/vegetal.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Tarta de vegetales</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Papas gajo">
        <img src="Views/img/vegetal.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Papas gajo</h4>
      </button>
    </div>
  </div>
  


   <div class="container" id="entradasdecarnes">
  <h1>Entradas carnes Frías</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Jamones y quesos">
        <img src="Views/img/carnes.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Jamones y quesos</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Carpaccio">
        <img src="Views/img/carnes.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Carpaccio</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Pate">
        <img src="Views/img/carnes.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Paté</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-dark text-white center" id="btnBebida" value="Salchichas a la diabla">
        <img src="Views/img/carnes.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Salchichas a la diabla</h4>
      </button>
    </div>
  </div>



    <div class="container" id="entradasmar">
  <h1>Entradas de Mariscos</h1>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="ceviche">
        <img src="Views/img/mar.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Ceviche</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Brochetas">
        <img src="Views/img/mar.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Brochetas</h4>
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="Surimi">
        <img src="Views/img/mar.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Surimi</h4>
      </button>
    </div>
    <div class="col-6 col-sm-6 center">
      <button class="card bg-warning center" id="btnBebida" value="charales">
        <img src="Views/img/mar.png" class="card-img-top icard" alt="...">
        <h4 class="card-title" id="entrada">Charales</h4>
      </button>
    </div>
  </div>