<div id="sideNav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#" onClick="guardar_comanda()" id="guardar">Insertar Comanda</a>
  <a href="editar_controller.php">Editar Comanda</a>
  <a href="#">Cobrar</a>
  <a href="logout_controller.php">Cerrar Sesión</a>
</div>


<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <button onClick="openNav()" class="btn btn-dark"><i id="side_icon" class="fas fa-arrow-right"></i></span></button>
  <a class="navbar-brand" href="#">Soulware</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          Platillos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#pizzas">Pizzas</a>
          <a class="dropdown-item" href="#pastas">Pastas</a>
          <a class="dropdown-item" href="#ensaladas">Ensaladas</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          Bebidas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#cervezas">Cervezas</a>
          <a class="dropdown-item" href="#refrescos">Refrescos</a>
          <a class="dropdown-item" href="#preparadas">Preparadas</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          Entradas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#vegetariana">Vegetariana</a>
          <a class="dropdown-item" href="#entradasdecarnes">Carne</a>
          <a class="dropdown-item" href="#entradasmar">Del Mar</a>
        </div>
      </li>
    </ul>
  </div>
</nav>



