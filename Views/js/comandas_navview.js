/* Set the width of the side navigation to 250px */
var cont_side_bar = 0;

function openNav() {

  if (cont_side_bar == 0) {
    document.getElementById("sideNav").style.width = "250px";
    document.getElementById("side_icon").className = "fas fa-arrow-left"
    cont_side_bar++;
  } else {
    document.getElementById("sideNav").style.width = "0";
    document.getElementById("side_icon").className = "fas fa-arrow-right"
    cont_side_bar = 0;
  }

}

/* snack bar para cada pedido agregado */
