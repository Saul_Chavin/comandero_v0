var orden_actual=""
var valueSelected = 0;
modal_content = document.getElementById("modal-p");
modal_header = document.getElementById("modal-title");
modal_icon = document.getElementById("modal-icon");
table = document.getElementById("tabla");
var combo = document.getElementById("folios");
//Evento para cambiar la tabla cuando seleccionas
//un folio nuevo->
$('select').on('change', function (e) {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  comanda = ordenes[valueSelected].split(",");
  table.innerHTML = "";
  for (let i = 0; i < comanda.length - 1; i++) {
    table.innerHTML += '<tr><td>' + comanda[i] + '</td></tr>';
  }
});

function guardar_comanda() {
  if (orden_actual) {
    modal_header.innerHTML = 'Orden Realizada';
    modal_content.innerHTML = 'El pedido fue agregado correctamente';
    modal_icon.innerHTML = '<i class="fas fa-check-circle font-green" id="icon"></i>'
    $.post("editar_controller.php", {
      editada: orden_actual+ordenes[valueSelected],
      folio: combo.options[combo.selectedIndex].text
    },
      function (data, status) {
      });
    orden_actual = "";
  } else {
    modal_header.innerHTML = 'Orden vacía';
    modal_content.innerHTML = 'No has seleccionado ningun producto';
    modal_icon.innerHTML = '<i class="fas fa-times-circle font-red" id="icon"></i>'
  }

}

//añadir platillo o bebida a la cadena que se enviara a la base de datos
$("button").click(function () {
  var fired_button = $(this).val();
  console.log(fired_button);
  if (fired_button) {
    orden_actual += fired_button + ",";
    alert(orden_actual);
  }

});