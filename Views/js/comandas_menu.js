var orden_actual = "";
modal_content = document.getElementById("modal-p");
modal_header = document.getElementById("modal-title");
modal_icon = document.getElementById("modal-icon");

//guarda la comanda en la base de datos
function guardar_comanda() {
  if (orden_actual) {
    modal_header.innerHTML='Orden Realizada';
    modal_content.innerHTML='El pedido fue agregado correctamente';
    modal_icon.innerHTML='<i class="fas fa-check-circle font-green" id="icon"></i>'
    $.post("orden_controller.php", {
      comanda: orden_actual
    },
      function (data, status) {
      });
    orden_actual = "";
  } else {
    modal_header.innerHTML='Orden vacía';
    modal_content.innerHTML='No has seleccionado ningun producto';
    modal_icon.innerHTML='<i class="fas fa-times-circle font-red" id="icon"></i>'
  }

}

//abrir el modal
$(document).ready(function () {
  $("#guardar").click(function () {
    $("#modal_success").modal("show");
  });
});

//añadir platillo o bebida a la cadena que se enviara a la base de datos
$("button").click(function () {
  var fired_button = $(this).val();
  console.log(fired_button);
  if (fired_button) {
    orden_actual += fired_button + ",";
    alert(orden_actual);
  }

});

