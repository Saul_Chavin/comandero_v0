<div class="container">
  <h1>Editar</h1>
  <form action="">
    <div class="row">
      <div class="form-group col-3">
        <select name="folio" id="folios" class="form-control">
          <?php
            for ($i=0; $i < count($folios); $i++) { 
              echo '<option value="'.$i.'">'.$folios[$i].'</option>';
            }
          ?>
        </select>
      </div>
      <div class="form-group col-3">

        <button type="button" class="btn btn-primary">Editar</button>

      </div>
      <div class="form-group col-3">

        <button type="button" class="btn btn-success" onClick="guardar_comanda()">Guardar</button>

      </div>
      <div class="form-group col-3">

        <button type="button" class="btn btn-danger">Salir</button>

      </div>
    </div>
  </form>
  <div class="row">
    <div class="col-12">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Pedidos</th>
          </tr>
        </thead>
        <tbody id="tabla">
          <?php
          //solo ejecuta el codigo la primera vez para mostrar en la tabla
            $elementos = explode("," , $ordenes[0]);
            foreach($elementos as $e){
              echo '<tr><td>'.$e.'</td></tr>';
            }
            ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row">
    <?php include('menu_view.php')?>
  </div>
</div>

<script type="text/javascript">
// pass PHP variable declared above to JavaScript variable
var ordenes = <?php echo json_encode($ordenes); ?> ;
/* console.log(ordenes); */
</script>